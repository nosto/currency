package com.fa.nosto.service.impl;

import com.fa.nosto.model.Currency;
import com.fa.nosto.service.RateService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class ExchangeRatesApiServiceTest {
    @Mock
    RateService rateService;

    private CurrencyConverterImpl currencyConverterImpl;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        currencyConverterImpl = new CurrencyConverterImpl(rateService);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void test() throws Exception {
        Double rate = 55.5555;
        Double amount = 11.1111;
        Mockito.doReturn(rate).when(rateService).getRate(Currency.USD, Currency.EUR);
        assertEquals( amount * rate, currencyConverterImpl.convert(amount, Currency.USD, Currency.EUR));
    }

}