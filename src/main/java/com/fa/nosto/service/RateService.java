package com.fa.nosto.service;

import com.fa.nosto.model.Currency;

public interface RateService {
    class RateServiceException extends Exception {
        public RateServiceException(String message) {
            super(message);
        }
        public RateServiceException(Exception reason) {
            super(reason);
        }
    }
    Double getRate(Currency from, Currency to) throws RateServiceException;
}
