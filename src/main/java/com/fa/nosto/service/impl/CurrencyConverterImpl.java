package com.fa.nosto.service.impl;

import com.fa.nosto.model.Currency;
import com.fa.nosto.service.CurrencyConvertor;
import com.fa.nosto.service.RateService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Service to do the real jon on calculation
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class CurrencyConverterImpl implements CurrencyConvertor {
    private final RateService rateService;

    @Override
    public List<Currency> currencies() {
        return Arrays.asList(Currency.values());
    }

    @Override
    public Double convert(@NonNull Double amount,
                          @NonNull Currency from,
                          @NonNull Currency to) {
        try {
            Double rate = rateService.getRate(from, to);
            return amount * rate;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
