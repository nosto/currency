package com.fa.nosto.service.impl;

import com.fa.nosto.model.Currency;
import com.fa.nosto.service.RateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

/**
 * Service to read rates from the third party.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ExchangeRatesApiService implements RateService {
    private final Environment env;

    private String requestTemplate;
    private String apiKey;
    private int connectTimeout;
    private int readTimeout;

    static final String URL_TEMPLATE="exchange_rates_api.url";
    static final String API_KEY="exchange_rates_api.key";
    static final String CONNECT_TIMEOUT="exchange_rates_api.connect_timeout.ms";
    static final String READ_TIMEOUT="exchange_rates_api.read_timeout.ms";


    @PostConstruct
    private void initRequestTemplate() {
        requestTemplate = env.getProperty(URL_TEMPLATE);
        apiKey = env.getProperty(API_KEY);
        readTimeout = env.getProperty(READ_TIMEOUT, Integer.class, 5000);
        connectTimeout = env.getProperty(CONNECT_TIMEOUT, Integer.class, 5000);
    }

    @Override
    public Double getRate(Currency from, Currency to) throws RateServiceException {
        try {
            String res = invokeApi(from, to);
            return parseResponse(res, to);
        } catch (Exception e) {
            throw new RateServiceException(e);
        }
    }

    private String invokeApi(Currency from, Currency to) throws Exception {
        URL url = new URL(String.format(requestTemplate, from, to));
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("apikey", apiKey);
        con.setRequestProperty("user-agent", "Application");
        con.setConnectTimeout(connectTimeout);
        con.setReadTimeout(readTimeout);

        log.info("Requesting: {}", url);
        int status = con.getResponseCode();

        InputStream in = status > 299 ? con.getErrorStream() : con.getInputStream();
        String text = new BufferedReader(
                new InputStreamReader(in, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
        if (status > 299) {
            log.error("Error {}: {}", status, text);
            throw new Exception(String.format("Requesting: %s caused Error %s: %s", url, status, text));
        }
        return text;
    }

    private Double parseResponse(String text, Currency to) {
        JSONObject resp = new JSONObject(text);
        JSONObject rates = resp.getJSONObject("rates");
        return rates.getDouble(to.name());
    }
}
