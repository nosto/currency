package com.fa.nosto.service;

import com.fa.nosto.model.Currency;
import lombok.NonNull;

import java.util.List;

/**
 * The interface for Currency Convertor.
 * Test assumption: lists of currencies supported by rate provider. (Hardcoded in the test)
 */
public interface CurrencyConvertor {
    /**
     * @return list of available currencies
     */
    List<Currency> currencies();

    /**
     * Estimates cost of converting <i>amount</i> of <i>from</i>
     * currency to <i>to</i>currency.
     * <p>
     * Example: if the current rate of 1 EUR = 1.25 USD
     * then convert(10, EUR, USD) will return 12.5.
     * </p>
     *
     * @param amount to covnert
     * @param from base currency
     * @param to target currency
     * @return amount * rate of base in target currency
     */
    Double convert(@NonNull Double amount,
                   @NonNull Currency from,
                   @NonNull Currency to);

}
