package com.fa.nosto.model;

/**
 * All available currencies. (Limited for test task)
 */
public enum Currency {
    EUR,
    CAD,
    CHF,
    CNY,
    GBP,
    JPY,
    USD,
    RUB
}
