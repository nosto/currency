package com.fa.nosto.controller;

import com.fa.nosto.model.Currency;
import com.fa.nosto.service.CurrencyConvertor;
import com.fa.nosto.service.impl.CurrencyConverterImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CurrencyConvertorController implements CurrencyConvertor {

    private final CurrencyConverterImpl currencyConverterImpl;

    @RequestMapping(value = "currencies")
    @Override
    public List<Currency> currencies() {
        return currencyConverterImpl.currencies();
    }

    @RequestMapping(value = "convert")
    @Override
    public Double convert(@RequestParam Double amount,
                          @RequestParam Currency from,
                          @RequestParam Currency to) {
        return currencyConverterImpl.convert(amount, from, to);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleError(HttpServletRequest req, Exception e) {
        log.error("Request: {} raised {}", req.getRequestURL(), e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}